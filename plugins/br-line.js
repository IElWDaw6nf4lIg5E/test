import Vue from 'vue';

Vue.filter('brLine', (value) => {
  return value.replace(/^\s+|\s+$/gm, "$&<br>")
})
