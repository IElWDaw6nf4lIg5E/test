import Vue from 'vue'

Vue.mixin({
  methods: {
    goToQuestionForm () {
      this.$scrollTo('#question-form');
    }
  }
})
