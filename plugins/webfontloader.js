// @link https://github.com/typekit/webfontloader
import WebFont from "webfontloader";

WebFont.load({
  google: {
    families: ["Open Sans:300,400,600:latin-ext"]
  }
});
