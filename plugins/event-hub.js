import Vue from 'vue'

const eventHub = new Vue();
//this helps WebStorm with autocompletion, otherwise it's not needed
//Vue.prototype.$eventHub = eventHub;

export default ({app}, inject) => {
  inject('eventHub', eventHub);
}
