import Vue from 'vue';

Vue.filter('wrapLines', function (value) {
  return value.replace(/.+$/gm, "<p>$&</p>");
})
