import Vue from 'vue';

Vue.filter('cleanHostname', (value) => {
  const url = new URL(value);
  return url.hostname
})
