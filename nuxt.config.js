import cs from './lang/cs-CZ.js';

export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: 'Virtuální čekárna',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'og:image', name: 'og:image', content: '/share-poradna.jpg' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#CA000A' },
  /*
  ** Global CSS
  */
  css: [
    '@fortawesome/fontawesome-svg-core/styles.css'
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/gtm',
  ],
  gtm: {
    id: 'GTM-PB9PZ4',
    /* Set to false to disable module in development mode */
    dev: true,
    scriptDefer: true,
    pageTracking: true,
  },
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    ["nuxt-i18n", {
      locales: [
        {
          code: "cs",
          iso: "cs-CZ",
          name: "Czech",
          file: "cs-CZ.js"
        },
      ],
      defaultLocale: "cs",
      vueI18n: {
        fallbackLocale: "cs",
        messages: { cs }
      },
      lazy: true,
      langDir: 'lang/'
    }],
    ['bootstrap-vue/nuxt'],
    ['vue-scrollto/nuxt', { x: false, }],
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: "~/plugins/br-line" },
    { src: '~/plugins/clean-hostname' },
    { src: '~/plugins/fontawesome' },
    { src: '~/plugins/global-mixins' },
    { src: "~/plugins/object-fit-images", ssr: false },
    { src: '~/plugins/v-calendar', ssr: false },
    { src: "~/plugins/webfontloader", ssr: false },
    { src: '~/plugins/wrap-lines' },
    { src: '~/plugins/vue-multiselect' },
    { src: "~/plugins/vue-the-mask" },
    { src: '~/plugins/vuelidate' },
    { src: '~/plugins/axios' },
    { src: '~/plugins/event-hub' },
  ],
  /*
  ** https://bootstrap-vue.org/docs
  */
  bootstrapVue: {
    bootstrapCSS: false,
    bootstrapVueCSS: false,
    components: ['BModal', 'BForm', 'BFormRadioGroup', 'BFormGroup', 'BFormInput'],
    directives: ['VBModal']
  },
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    baseURL: process.env._AXIOS_BASE_URL_
  },
  /*
  ** Router configuration
  */
  router: {
    linkActiveClass: 'is-active',
    linkExactActiveClass: 'is-exact-active',
    base: process.env.ROUTER_BASE_URL
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  },

  serverMiddleware: [
    { path: '/api', handler: '~/api/index.js' }
  ]

}
