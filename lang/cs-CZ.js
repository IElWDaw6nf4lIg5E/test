export default {
  appName: "Virtuální čekárna",
  imNotDoctor: "Nemám praktického lékaře",
  imNotDoctorPerex: "Nemáte svého lékaře? Ve Virtuální čekárně je vám k dispozici 300 lékařů z týmu uLékaře.cz, kteří jsou připraveni vám pomoci.",
  termsOfUse: "Podmínky užití",
  goToHome: "Zpět na hlavní stránku",
  alertInfo: "Virtuální čekárna České republiky nenahrazuje záchrannou službu. <strong class='text-primary'>Při akutních potížích volejte linku 155.</strong>",
  choose: "Vybrat",
  close: "Zavřít",
  yes: "Ano",
  no: "Ne",
  proLekare: {
    title: "Pro lékaře",
    link: "https://www.prolekare.cz/virtualni-cekarna"
  },
  uLekare: {
    title: "uLékaře.cz",
    perex: "Největší česká medicínská online poradna a průkopník v oblasti telemedicíny si klade za cíl zefektivnit a zkvalitnit české zdravotnictví. Virtuální čekárna propojuje pacienty s jejich praktickými lékaři a umožňuje vzdálenou a pohodlnou konzultaci zdravotních potíží."
  },
  hello: {
    title: "Virtuální čekárna",
    subTitle: "České republiky",
    perex: "Nabízíme vám <strong>pohodlnou konzultaci s vaším praktickým lékařem</strong>, abyste mohli začít hned, a přitom vzdáleně řešit jakýkoli zdravotní problém. \nVstupte do Virtuální čekárny odkudkoli a kdykoli a ušetřete si cestu do ordinace, není-li nezbytně nutná.",
  },
  partners: {
    specialSupport: "Speciální podpora",
    generalPartners: "Generální partneři",
    otherPartners: "Partneři",
    thankYouPartners: "Děkujeme za podporu našim partnerům",
    info: "Služby Virtuální čekárny \n<strong class='text-primary text-bold'>jsou v období pandemie zdarma</strong> \ndíky partnerům."
  },
  howItWorks: {
    title: "Jak vám pomůžeme",
    items: [
      {
        icon: "dot-comment.svg",
        title: "Vstupte do čekárny",
        perex: "Napište svému praktickému lékaři, jakou pomoc potřebujete. \nVyplňte prosím dotazník bezinfekčnosti a připojte informaci o své časové dostupnosti pro případ návštěvy v ordinaci."
      },
      {
        icon: "dot-doctor.svg",
        title: "Počkejte na odpověď",
        perex: "Lékař si vaši zprávu přečte a odpoví na ni e-mailem hned, jakmile bude online. \nPokud bude třeba, domluví se s vámi na termínu osobní návštěvy."
      },
      {
        icon: "dot-cap.svg",
        title: "Využijte výhody",
        perex: "Vzdálená konzultace s praktickým lékařem je pohodlná a bezpečná. \nPro případ návštěvy v ordinaci lékař bude o vašich potížích předem informován a návštěva tak proběhne efektivněji."
      },
    ]
  },
  ContactDoctor: {
    title: "Spojte se s praktickým lékařem",
  },
  findDoctor: {
    btnLink: "Vyhledat lékaře",
    placeholder: "Zadejte jméno svého lékaře",
  },
  multipleOrdinations: 'Více ordinací',
  infoForDoctor: {
    text: "V těchto dnech se praktičtí lékaři do Virtuální čekárny průběžně registrují. Nenajdete-li svého lékaře v seznamu, můžete díky našim sponzorům zdarma využít možnost vzdáleně řešit jakýkoliv zdravotní problém s jedním z 300 lékařů týmu uLékaře.cz.",
    btnTitle: "Položit dotaz",
  },
  benefits: {
    benefitsConsultation: {
      title: "Výhody vzdálené konzultace a objednání",
      infoList: [
        {
          text: "Své zdravotní potíže můžete konzultovat kdykoli a odkudkoli.",
          checked: true
        },
        {
          text: "Lékař vám odpoví, jakmile bude online.",
          checked: true
        },
        {
          text: "Pokud lékař na základě vašich potíží doporučí osobní návštěvu v ordinaci, rovnou vám zašle termín návštěvy.",
          checked: true
        },
        {
          text: "Odpověď lékaře získáte vždy a cesta k ní je bezpečná.",
          checked: true
        },
      ]
    },
    benefitsQuestion: {
      title: "Výhody vzdálené konzultace s lékaři z týmu uLékaře.cz",
      infoList: [
        {
          text: "Své zdravotní problémy můžete konzultovat kdykoli a odkudkoli, odpověď lékaře získáte vždy a cesta k ní je bezpečná.",
          checked: true
        },
        {
          text: "Odpověď obdržíte do 24 hodin od odeslání dotazu na vámi zadanou e-mailovou adresu.",
          checked: true
        },
        {
          text: "Pokud lékař na základě popisu vašich potíží doporučí osobní návštěvu v ordinaci, zašleme vám nabídku zdravotnických zařízení, kam je možné se objednat.",
          checked: true
        },
        {
          text: "Na dotazy týkající se onemocnění Covid-19 vám odpovíme  <a href='https://www.ulekare.cz/poradna/koronavirus-10' target=”_blank”>ve specializované poradně</a>.",
          checked: false
        },
      ]
    },
    benefitsOnline: {
      title: "Výhody online poradny",
      infoList: [
        {
          text: "Váš praktický lékař odpoví nejpozději další pracovní den. Odpověď vám zašleme na email.",
          checked: true
        },
        {
          text: "V případě, že lékař doporučí osobní návštěvu, zašleme vám možné termíny návštěvy ",
          checked: true
        },
        {
          text: "Zdarma díky parnterům",
          checked: true
        },
      ]
    }
  },
  doctorDetail: {
    note: "Aktuální informace:",
    noteInfo: "Ordinační hodiny zůstávají stejné i v době pandemie. Přijímám pacienty pouze v rouškách.",
    sendQuestionThisDoctor: "Položit dotaz tomuto lékaři",
    sendQuestionOurDoctor: "Položit dotaz našim lékařům",
    sendQuestionInfo: "Dotaz položte, jen pokud se jedná o <strong>vašeho praktického lékaře</strong>.",
    surgery: "Ordinace",
    OfficeHoursIsEmpty: "Ordinační hodiny lékař nevyplnil",
    empty: "Nevyplněno",
    dontHaveVirtualRoom: {
      title: "Tento lékař nemá aktivovanou službu Virtuální čekárny",
      titleTmp: "Váš praktický lékař vám bude k dispozici od 1. 6. 2020",
      perex: "Potřebujete-li vzdálenou konzultaci hned, můžete zatím díky našim sponzorům zdarma využít možnost řešit jakýkoli zdravotní problém s jedním z 300 lékařů týmu uLékaře.cz."
    },
  },
  questionForm: {
    title: "Váš dotaz lékaři",
    subtitle: "Co vás trápí, jak dlouho problém trvá, berete nějaké léky, proběhlo již nějaké vyšetření, ...",
    modalTitle: "Přiložte foto nebo lékařské zprávy",
    send: "Odeslat lékaři",
    back: "Zpět k dotazu",
    sendTitle: "Váš dotaz byl úspěšně odeslán",
    next: "Pokračovat",
    sendPerex: "Děkujeme za vaši návštěvu ve Virtuální čekárně.",
    question: {
      placeholder: 'Sem napište, co vás trápí',
      errorText: 'Dotaz na lékaře je povinný, prosím, vyplňte ho.'
    },
    firstName: {
      title: 'Jméno',
      helpText: 'Křestní jméno zobrazíme u dotazu na stránkách.',
      errorText: 'Jméno je povinné, prosím, vyplňte ho.'
    },
    lastName: {
      title: 'Příjmení',
      helpText: 'Příjmení je třeba ke komunikaci se zdravotnickým týmem.',
      errorText: 'Příjmení je povinné, prosím, vyplňte ho.'
    },
    email: {
      title: 'E-mail',
      helpText: 'Prostřednictvím e-mailu s vámi budeme komunikovat.',
      errorText: 'E-mail je povinný, prosím, vyplňte ho.',
      errorEmailText: 'E-mail nemá správný formát.'
    },
    phone: {
      title: 'Telefon',
      helpText: 'Sestřička vám může nabídnout objednání k lékaři.',
      errorText: 'Telefonní číslo je povinné, prosím, vyplňte ho.',
      errorPhoneText: 'Zadejte telefonní číslo (+420 123456789).'
    },
    birthYear: {
      title: 'Rok narození',
      titleRegistered: 'Rok narození pacienta',
      helpText: 'Rok narození osoby, které se problém týká.',
      errorText: 'Rok narození je povinný, prosím, vyplňte ho.',
      errorLengthText: 'Rok narození napište v čtyřčíselném formátu.',
      errorBetweenText: 'Napište skutečný rok narození.'
    },
    zipCode: {
      title: 'PSČ',
      helpText: 'Podle PSČ vám doporučíme, kam se objednat.',
      errorText: 'PSČ je povinné, prosím, vyplňte ho.',
      errorZipCodeText: 'PSČ nemá správný formát.',
    },
    insuranceCompany: {
      title: 'Pojišťovna',
      errorText: 'Pole je povinné. Prosím vyberte pojišťovnu.',
      selectInsuranceCompany: 'Vyberte vaši pojišťovnu'
    },
    timeAvailability: {
      title: 'Vaše časová dostupnost',
      errorText: 'Pole je povinné. Prosím vyplňte časovou dostupnost.',
      placeholder: 'Napište, jaké máte časové možnosti v následujících dnech'
    },
    selectedBranchValue: {
      title: 'Ordinace',
      selectInsuranceCompany: 'Vyberte ordinaci'
    },
    personalId: {
      title: "Číslo pojištěnce",
      helpText: "Slouží k ověření, že jste v evidenci svého lékaře.",
      errorText: 'Pole je povinné. Prosím vyplňte číslo pojištěnce.',
    },
    checkedMyDoctor: "Zkontroloval jsem, že se jedná o mého praktického lékaře"
  },
  fileUpload: {
    initial: "Kliknutím vyberte soubory nebo je přetáhněte myší.",
    send: 'Odeslat',
    uploadAgain: 'Zkuste to prosím znovu',
    addFiles: "Přiložte foto nebo lékařské zprávy.",
    acceptedTypes: 'Povolené typy jsou obrázky (.jpg, .png), dokumenty v PDF, zvuk v MP3 a video (.avi, .mp4)',
    typeError: "Tento typ souboru není podporován. Soubory prosím nahrajte v uvedených formátech .jpg, .png,.pdf, .mp3, .mp4, .avi.",
    sizeError: "Překročena maximální povolená velikost. Maximální povolená velikost souboru je 2 MB, maximální povolená velikost všech souborů dohromady je 50 MB."
  },
  results: {
    title: "Spojte se se svým praktickým lékařem",
    resultInfoStart: "Na hledaný výraz",
    resultInfoEnd: "bylo nalezeno {count} výsledků",
    noResultInfoEnd: "nebyl nalezen žádný lékař",
    noResultInfoPerex: "Zkontrolujte, že jste jméno lékaře zadali správně, případně můžete zkusit hledat méně konkrétně (např. dle křestního jména) a výsledky můžete v dalším kroku filtrovat podle lokality.",
    startSearch: "Zadejte hledaný výraz",
    moreResults: "Zobrazit další lékaře",
    doctorNotInResults: "Můj praktický lékař není v seznamu",
    filterInfo: "Výsledky vyhledávání můžete upřesnit zadáním <strong>lokality ordinace svého praktického lékaře</strong>",
    filterDefaultText: "Celá Česká republika",
    loadingInfo: "Vyhledávám...",
    showAll: "Zobrazit všechny výsledky ({totalCount})"
  },
  iDontHaveDoctor: {
    title: "Nemáte praktického lékaře? Nevadí!",
    perex: "Díky našim sponzorům můžete zdarma využít možnost vzdáleně řešit jakýkoli zdravotní problém s jedním z 300 lékařů týmu uLékaře.cz, kteří jsou připraveni vám pomoci.",
    sendQuestion: "Položit dotaz našim lékařům",
  },
  doctorIsNotInList: {
    title: "Nenašli jste svého praktického lékaře v naší čekárně? Nevadí!",
    sendQuestion: "Položit dotaz našim lékařům",
  },
  virtualOffice: {
    info: "Od 1.6. se budete moci podívat do své virtuální ordinace"
  },
  footer: {
    copyright: "&copy; 2007-{thisYear} uLékaře.cz, s.r.o.",
    issn: "ISSN 1802-5544",
    companyTitle: "Společnost uLékaře.cz",
    companyList: [
      {
        title: "Kontakt",
        link: "https://www.ulekare.cz/kontakt"
      },
      {
        title: "Kariéra",
        link: "https://www.ulekare.cz/kariera"
      },
      {
        title: "Inzerce",
        link: "https://www.ulekare.cz/inzerce"
      },
      {
        title: "Zaměstnanecký benefit zdraví",
        link: "https://www.ulekare.cz/zamestnanecky-benefit-zdravi"
      },
      {
        title: "Zásady zpracování osobních údajů",
        link: "https://www.ulekare.cz/zasady-zpracovani-osobnich-udaju"
      },
      {
        title: "Podmínky užití",
        link: "/podminky-uziti"
      },
      {
        title: "Prohlášení o cookies",
        link: "/cookies"
      },
    ]
  },
  dataProcessing : {
    iAgree: "Souhlasím se",
    personalDataProcessing: "zpracováním osobních údajů",
    personalDataTitle: "Souhlas se zpracováním osobních údajů včetně zvláštní kategorie osobních údajů",
    personalDataText: "Správce zpracovává osobní údaje uživatelů, a to včetně údajů zvláštní kategorie, které mu uživatelé sdělí, pro účely využití služby uLékaře.cz (jak je popsána na webu ulekare.cz či v mobilní aplikaci a v souladu s Podmínkami užití, které naleznete zde. \nZpracování osobních údajů uživatelů probíhá vždy a pouze na základě uděleného souhlasu se zpracováním osobních údajů (včetně zpracování údajů zvláštní kategorie), jehož text naleznete zde a který je udělován uživatelem (subjektem údajů) při registraci do služby uLékaře.cz.",
    newsletterProcessing: "zasíláním Zpravodaje uLékaře.cz",
    newsletterText: "Souhlasím se zasíláním Zpravodaje uLékaře.cz (se zprávami o společnosti a projektu uLékaře.cz, s.r.o., nebo o produktech a službách spolupracujících třetích osob). Souhlas se zasíláním odvoláte volbou ve Zpravodaji nebo na adrese <a href=’mailto:osobniudaje@ulekare.cz’>osobniudaje@ulekare.cz</a>.",
    conditionsAgreement: "podmínkami užití služby Virtuální čekárna",
  },
  legalNotice: {
    title: "Podmínky užití",
  },
  days: {
    monday: 'Po',
    tuesday: 'Út',
    wednesday: 'St',
    thursday: 'Čt',
    friday: 'Pá',
    saturday: 'So',
    sunday: 'Ne'
  },
  error: {
    404: "Stránka nenalezena",
    400: "Chyba 400",
    500: "Chyba 500",
    universal: "Vyskytla se chyba",
  },
  cookiePolicy: {
    message: "Pro provoz služby uLékaře.cz využíváme soubory cookies. Využíváním našeho webu s jejich používáním souhlasíte.",
    agree: "Souhlasím",
    moreInfo: "Více informací zde",
    infoTitle: "Prohlášení o cookies",
    infoText: "Služba uLékaře.cz využívá pro svou činnost tzv. cookies. Cookie je krátký textový soubor, který navštívená webová stránka odešle do prohlížeče. Soubory cookie slouží k celé řadě účelů. \nPoužíváme je například k ukládání údajů reklamních systémů, ke sledování pohybu a chování návštěvníků na stránkách a k nastavení personalizace webu, např. pomáhají při Vašem přihlášení do služby uLékaře.cz. Pokud je tedy používáte, mohou si zapamatovat Vaše přihlašovací údaje, prostředí (např. prohlížeč) a funkce, abyste své přihlašovací údaje nemuseli pokaždé zadávat. \nStandardní webové prohlížeče (Internet Explorer, Mozilla Firefox, Google Chrome apod.) podporují správu cookies. V rámci nastavení prohlížečů můžete jednotlivé cookie ručně mazat, blokovat či zcela zakázat jejich použití, lze je také blokovat nebo povolit jen pro jednotlivé internetové stránky. Pro detailnější informace prosím použijte nápovědu Vašeho prohlížeče. \nPovolením cookies souborů v nastavení vašeho prohlížeče souhlasíte s používáním cookies souborů na našich stránkách.",
  },
  meta: {
    hp: {
      title: "Propojujeme pacienty s lékaři │ Virtuální čekárna",
      description: "Nabízíme vám pohodlnou konzultaci s vaším praktickým lékařem, abyste mohli začít hned, a přitom vzdáleně řešit jakýkoli zdravotní problém. Vstupte do Virtuální čekárny odkudkoli a kdykoli a ušetřete si cestu do ordinace, není-li nezbytně nutná.",
      ogTitle: "Virtuální čekárna České republiky"
    },
    searchResults: {
      title: "Vyhledávání │ Virtuální čekárna",
      description: "",
      ogTitle: "Spojte se se svým praktickým lékařem"
    },
    dontHaveDoctor: {
      title: "Nemáte praktického lékaře? │ Virtuální čekárna",
      description: "Díky našim sponzorům můžete zdarma využít možnost vzdáleně řešit jakýkoli zdravotní problém s jedním z 300 lékařů týmu uLékaře.cz, kteří jsou připraveni vám pomoci.",
      ogTitle: "Nemáte praktického lékaře? Nevadí!"
    },
    doctorNotInList: {
      title: "Nenašli jste svého praktického lékaře? │ Virtuální čekárna",
      description: "Díky našim sponzorům můžete zdarma využít možnost vzdáleně řešit jakýkoli zdravotní problém s jedním z 300 lékařů týmu uLékaře.cz, kteří jsou připraveni vám pomoci.",
      ogTitle: "Nenašli jste svého praktického lékaře v naší čekárně? Nevadí!"
    },
    doctorDetail: {
      title: "{doctorName} │ Virtuální čekárna",
      description: "Díky našim sponzorům můžete zdarma využít možnost vzdáleně řešit jakýkoli zdravotní problém s jedním z 300 lékařů týmu uLékaře.cz, kteří jsou připraveni vám pomoci.",
      ogTitle: "Nenašli jste svého praktického lékaře v naší čekárně? Nevadí!"
    },
    cookies: {
      title: "Prohlášení o cookies │ Virtuální čekárna",
      description: "",
      ogTitle: ""
    },
    legalNotice: {
      title: "",
      description: "",
      ogTitle: ""
    },
    rsasistant: {
      title: "RS asistent │ uLékaře.cz",
      description: "Program nabízí praktickou podporu v léčbě a doplňkovou péči, různorodé informace k celkové podpoře zdraví a života se závažným autoimunitním onemocněním.",
      ogTitle: "RS ASISTENT pomáhá pacientům s roztroušenou sklerózou"
    }
  },
  infectionTest: {
    title: "Infekční test",
    perex: "Aby čekárna byla bezpečná pro vás i praktického lékaře, <strong>vyplňte prosím pravdivě</strong> následující údaje:",
    selectCountry: "Uveďte zemi",
    selectDate: "Uveďte datum",
    questions: {
      fever: "Horečka (nad 38 st. C)",
      dryCough: "Kašel",
      breathingDifficulties: "Dýchací obtíže",
      musclePain: "Bolest svalů či kloubů",
      sneezing: "Kýchání",
      infectedContact: "V posledních 14 dnech kontakt s nakaženou osobou",
      visitRiskCountry: "Návštěva rizikové země",
      riskCountry: {
        placeholder: "Uveďte zemi",
        errorText: "Pole je povinné. Prosím uveďte zemi.",
        label: 'Země'
      },
      riskDate: {
        placeholder: "Uveďte datum",
        errorText: "Pole je povinné. Prosím uveďte datum.",
        label: 'Termín návštěvy'
      }
    }
  },
  rsAssistant: {
    projectPartner: "Partner projektu",
    hello: {
      title: "RS ASISTENT",
      subTitle: "Váš",
      perex: "Vítejte na stránce programu <strong>RS ASISTENT</strong>, který Vám nabízí doplňkovou péči a praktickou podporu v nové léčbě Vašeho onemocnění. Program zahrnuje pravidelná SMS a telefonická připomenutí užití léku a možnost objednání u fyzioterapeuta a psychologa. Dále nabízí různorodé informace k celkové podpoře Vašeho zdraví a života s onemocněním na <a href='https://www.zivotvhlavniroli.cz' target='_blank'>pacientském webu</a>. \nRegistrovat se do programu můžete prostřednictvím online formuláře na této stránce. Pokud s jeho vyplněním potřebujete pomoci nebo máte jakékoliv dotazy související s registrací, pomůžeme Vám na tel. <a href='tel:00420730701431'>730 701 431</a> či na e-mailu <a href='mailto:rs-asistent@ulekare.cz'>rs-asistent@ulekare.cz</a>.",
      imgTitle: "Sestra Novartis"
    },
    scrollToRegistration: "Přejít na registraci do programu",
    registerForm: {
      title: "Registrace do programu",
      checkTitle: "Jakou léčbou Vás budeme provázet? (Výběr léku)",
      formTitle: "Vyplňte své kontaktní údaje",
      send: "Odeslat",
      sendTitle: "Registrace do programu RS ASISTENT proběhla úspěšně",
      sendPerex: "Těší nás Vaše registrace do podpůrného programu RS ASISTENT.<br/>Vyčkejte na telefonát zdravotní sestřičky, která s Vámi domluví podrobnosti související s nastavením programu na míru Vašim potřebám.",
      sendNextmessage: "Poslat novou zprávu",
      firstName: {
        title: 'Jméno',
        errorText: 'Jméno je povinné, prosím, vyplňte ho.'
      },
      lastName: {
        title: 'Příjmení',
        errorText: 'Příjmení je povinné, prosím, vyplňte ho.'
      },
      email: {
        title: 'E-mail',
        errorText: 'E-mail je povinný, prosím, vyplňte ho.',
        errorEmailText: 'E-mail nemá správný formát.'
      },
      phone: {
        title: 'Telefon',
        errorText: 'Telefonní číslo je povinné, prosím, vyplňte ho.',
        errorPhoneText: 'Zadejte telefonní číslo (+420 123456789).'
      },
      birthYear: {
        title: 'Rok narození',
        titleRegistered: 'Rok narození pacienta',
        errorText: 'Rok narození je povinný, prosím, vyplňte ho.',
        errorLengthText: 'Rok narození napište v čtyřčíselném formátu.',
        errorBetweenText: 'Napište skutečný rok narození.'
      },
      zipCode: {
        title: 'PSČ',
        errorText: 'PSČ je povinné, prosím, vyplňte ho.',
        errorZipCodeText: 'PSČ nemá správný formát.',
      },
      treatmentCenter: {
        title: 'Ve kterém MS centru pro diagnostiku a léčbu roztroušené sklerózy se aktuálně léčíte?',
        selectMsCentrum: 'Vyberte centrum',
        helpText: '',
        errorText: 'Tato položka je vyžadována',
      },
      medication: {
        mayzent: "Mayzent",
        gilenya: "Gilenya",
        errorText: 'Tato položka je vyžadována',
      },
    },
    dataProcessing : {
      iAgree: "Zaškrtnutím tohoto políčka souhlasíte",
      iAgreePersonal: "Souhlasím se zpracováním mých osobních údajů, a to včetně zvláštní kategorie údajů o zdravotním stavu tak, jak je uvedeno v ",
      personalDataProcessing: "Zásadách zpracování osobních údajů",
      personalDataTitle: "Zaškrtnutím tohoto políčka souhlasíte s podmínkami užití služby RS Asistent.",
      personalDataText: "Správce zpracovává osobní údaje uživatelů, a to včetně údajů zvláštní kategorie, které mu uživatelé sdělí, pro účely využití služby uLékaře.cz (jak je popsána na webu ulekare.cz či v mobilní aplikaci a v souladu s Podmínkami užití, které naleznete zde. \nZpracování osobních údajů uživatelů probíhá vždy a pouze na základě uděleného souhlasu se zpracováním osobních údajů (včetně zpracování údajů zvláštní kategorie), jehož text naleznete zde a který je udělován uživatelem (subjektem údajů) při registraci do služby uLékaře.cz.",
      conditionsAgreement: "s podmínkami užití služby RS Asistent",
      modalTitle: "Souhlasím s podmínkami užití služby RS Asistent"
    },
    personalModal: {
      modalTitle: "Souhlasím se zpracováním osobních údajů služby RS Asistent"
    },
    alertInfo: "Vše, co potřebujete vědět o programu RS ASISTENT, se <a href='https://www.ulekare.cz/media/rs_asistent.pdf' target='_blank'>dočtete zde</a>.",
    footer: {
      perex: "Největší česká medicínská online poradna a průkopník v oblasti telemedicíny si klade za cíl zefektivnit a zkvalitnit české zdravotnictví.<br/><br/>CZ2007840017/07/2020"
    },
    formInfo: '"SMS připomínka" užití léků je služba pouze informačního charakteru. Prosím, berte na vědomí, že pro užívání léků jsou zásadní a rozhodující informace, které jste obdržel od svého lékaře nebo lékárníka. Poskytovatel služby "SMS připomínka" nenese odpovědnost za stav vašeho telefonu nebo signálu pro příjem sms zpráv.'
  }
}
