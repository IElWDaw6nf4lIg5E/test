export const state = () => ({
  doctors: [],
  loading: false,
  searchedData: [],
  pagination: {},
  filters: {},
  source: {},
  doctorDetail: {}
})

export const mutations = {
  setDoctors(state, doctors) {
    state.doctors = doctors
  },
  addDoctors(state, doctors) {
    state.doctors = state.doctors.concat(doctors)
  },
  setLoading(state, payload) {
    state.loading = payload
  },
  setSearchedData(state, payload) {
    state.searchedData = payload
  },
  setPagination(state, payload) {
    state.pagination = payload
  },
  setFilters(state, payload) {
    state.filters = payload
  },
  setSource(state, payload) {
    state.source = payload
  },
  setDoctorDetail(state, payload) {
    state.doctorDetail = payload
  }
}

export const actions = {
  async search({ commit, state }, payload) {
    //dont search when searching for string that was searched before
    if (
      state.searchedData.searchValue == payload.searchValue &&
      state.searchedData.page == payload.page &&
      state.searchedData.limit >= payload.limit
    ) return

    //if loading first page, clear doctors and pagination
    if (typeof payload.page == 'undefined' ||  payload.page == 1) {
      commit('setDoctors', [])
      commit('setPagination', [])
    }
    //dont search for string.length < 3 - it is important to keep setDoctors and setPagination commit to [] before this call
    if (payload.searchValue.length < 3) return

    let url = '/api/v2/doctors/search?'
    let params = []

    if (payload.searchValue){
      params.push('search='+payload.searchValue)
    }
    if (payload.limit){
      params.push('limit='+payload.limit)
    }
    if (payload.page){
      params.push('page='+payload.page)
    }
    if (payload.filter){
      params.push('filter={"region":"'+payload.filter+'"}')
    }

    if (!(Object.keys(state.source).length === 0 && state.source.constructor === Object) && typeof state.source.cancel === 'function') {
      state.source.cancel()
    }

    commit('setLoading', true)

    const source = this.$axios.CancelToken.source()
    commit('setSource',source)

    await this.$axios.get(url+params.join('&'),{
      cancelToken: source.token,
    })
      .then((results) => {
        if (results.data.pagination.page > 1){
          //loading more doctors
          commit('addDoctors', results.data.doctors)
        }else {
          //first page - need to override doctors
          commit('setDoctors', results.data.doctors)
        }
        commit('setPagination', results.data.pagination)
        commit('setFilters', results.data.filter)
        commit('setLoading', false)
      })
      .catch(error => {
        if (this.$axios.isCancel(error)) {
          // TODO CANCLED - toto asi chcem dat prec
        } else {
          commit('setDoctors', {})
          commit('setPagination', {})
          commit('setFilters', {})
          commit('setLoading', false)
        }
      })
  },
  async getDoctor({ commit, state }, payload) {
    let url = '/api/v2/doctors/'+payload.id
    if (payload.req) {
      url = process.env.SERVER_API_URL+'/v2/doctors/'+payload.id
    }
    return await this.$axios.get(url)
      .then(res => {
        commit('setDoctorDetail', res.data)
        return res.data
      }).catch(err => {
        return {}
      })
  }
}
