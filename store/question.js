export const state = () => ({
  attachments: [],
  loading: false,
  insuranceCompanyList: [],
  countryList: [],
  selectedBranchValue: null
})

export const getters = {
  getSelectedBranchValue(state) {
    return state.selectedBranchValue
  }
}

export const mutations = {
  addAttachment(state, payload) {
    state.attachments.push(payload)
  },
  deleteAttachment(state, payload) {
    state.attachments.splice(payload, 1)
  },
  resetAttachments(state) {
    state.attachments = []
  },
  setLoading(state, payload) {
    state.loading = payload
  },
  setInsuranceCompanyList(state, payload) {
    state.insuranceCompanyList = payload
  },
  setCountryList(state, payload) {
    state.countryList = payload
  },
  setSelectedBranchValue(state, payload) {
    state.selectedBranchValue = payload
  }
}

export const actions = {
  loadInsuranceCompanyList({commit}) {
    this.$axios.get('/api/v2/health-insurance-companies/search')
      .then(data => {
        commit('setInsuranceCompanyList', data.data)
      })
      .catch(err => {
      })
  },

  loadCountryList({commit}) {
    this.$axios.get('/api/v2/country')
      .then(data => {
        commit('setCountryList', data.data)
      })
      .catch(err => {
      })
  },

  loadPreScoringForm({commit}) {
    this.$axios.get('/api/v2/health-insurance-companies/search')
      .then(data => {
        //commit('setInsuranceCompanyList', data.data)
      })
      .catch(err => {

      })
  }

}
