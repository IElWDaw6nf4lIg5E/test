export const state = () => ({
  doctors: [],
  loading: false,
  searchedData: [],
  pagination: {},
  source: {}
})

export const mutations = {
  setDoctors(state, doctors) {
    state.doctors = doctors
  },
  setLoading(state, payload) {
    state.loading = payload
  },
  setSearchedData(state, payload) {
    state.searchedData = payload
  },
  setPagination(state, payload) {
    state.pagination = payload
  },
  setSource(state, payload) {
    state.source = payload
  }
}

export const actions = {
  search({ commit, state }, payload) {
    if (
      state.searchedData.searchValue == payload.searchValue &&
      state.searchedData.page == payload.page &&
      state.searchedData.limit >= payload.limit
    ) return

    commit('setSearchedData',payload)
    commit('setDoctors', [])
    commit('setPagination', [])

    //dont search for string.length < 3 - it is important to keep setDoctors and setPagination commit to [] before this call
    if (payload.searchValue.length < 3) return

    let url = '/api/v2/doctors/search?'
    let params = []

    if (payload.searchValue){
      params.push('search='+payload.searchValue)
    }
    if (payload.limit){
      params.push('limit='+payload.limit)
    }
    if (payload.page){
      params.push('page='+payload.page)
    }
    if (!(Object.keys(state.source).length === 0 && state.source.constructor === Object)) {
      state.source.cancel()
    }

    commit('setLoading', true)

    const source = this.$axios.CancelToken.source()
    commit('setSource',source)

    this.$axios.get(url+params.join('&'),{
      cancelToken: source.token,
    })
      .then((results) => {
        commit('setDoctors', results.data.doctors)
        commit('setPagination', results.data.pagination)
        commit('setLoading', false)
      })
      .catch(err => {
        if (!this.$axios.isCancel(err)) {
          commit('setDoctors', {})
          commit('setPagination', {})
          commit('setLoading', false)
        }
      })
  }
}
