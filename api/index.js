const axios = require("axios")
const dotenv = require('dotenv')
const crypto = require('crypto')
const bodyParser = require('body-parser')
const fs = require('fs')

dotenv.config()
axios.defaults.headers['X-Api-Key'] = process.env.API_KEY

const app = require('express')()
app.use(
  bodyParser.urlencoded({
    limit: '50mb',
    extended: true
  })
)
app.use(bodyParser.json({
    limit: '50mb',
    extended: true
  })
)

module.exports = { path: '/api', handler: app }

function logError(err) {
  let date = new Date()
  let data = '\n['+date.getHours()+':'+date.getMinutes()+':'+date.getSeconds()+'] '
  if (err != undefined && err.response !== undefined) {
    data += err.response
  }else{

  }
  fs.appendFile(__dirname+'/../logs/api_error_'+date.getFullYear()+'_'+(date.getMonth()+1)+'_'+date.getDate()+'.log',data, err => {
    //console.log('err from writeFiles', err)
  });
}

function returnResponse(res, code, data = '') {
  res.status(code)
  res.setHeader('Content-Type', 'application/json')
  res.send(data)
}

async function getToken(api_client_id = 1) {
  let api_key = process.env.API_KEY
  let private_key = process.env.PRIVATE_KEY

  if (api_client_id == 61){
    private_key = process.env.PRIVATE_KEY_APP
    api_key = process.env.API_KEY_APP
  }

  const partnerHash = crypto.createHash('md5')
    .update((private_key + Math.floor(Date.now() / 60)))
    .digest("hex")
  return await axios.post(process.env.SERVER_API_URL + '/v2/token',{
    "partnerId": 0,
    "partnerHash": partnerHash,
    "partnerClientId": api_client_id
  }, {
    headers: {
      'X-Api-Key': api_key
    }
  }).then(data => {
    return data.data.apiToken
  }).catch( err => {
    logError(err)
    return err
  })
}

app.post('/v2/questions', async (req, res) => {
  let token
  try {
    token = await getToken()
  } catch (err){
    returnResponse(res, err.response.status)
    return
  }
  axios.defaults.headers['X-Api-Token'] = token

  axios.post(process.env.SERVER_API_URL + req.url,req.body)
    .then(data => returnResponse(res,200, data.data))
    .catch(err => {
      returnResponse(res, err.response.status)
    })
})

app.get('/v2/doctors/search', (req, res) => {
  axios.get(process.env.SERVER_API_URL + req.url)
    .then(data => returnResponse(res,200, data.data))
    .catch(err => {
      returnResponse(res, err.response.status, err.response.data)
    })
})

app.get('/v2/doctors/:id', (req, res) => {
  axios.get(process.env.SERVER_API_URL + req.url)
    .then(data => returnResponse(res, 200, data.data))
    .catch(err => {
      returnResponse(res, err.response.status,err.response.data)
    })
})

app.get('/v2/health-insurance-companies/search', async (req, res) => {
  let token
  try {
    token = await getToken()
  } catch (err){
    returnResponse(res, err.response.status)
    return
  }
  axios.defaults.headers['X-Api-Token'] = token
  axios.get(process.env.SERVER_API_URL + req.url, {
    headers:{
      'Content-Type': 'application/json'
    }
  })
    .then(data => returnResponse(res,200, data.data))
    .catch(err => returnResponse(res, err.response.status, err.response.data))
})

app.get('/v2/country', async (req, res) => {
  let token
  try {
    token = await getToken()
  } catch (err){
    returnResponse(res, err.response.status)
    return
  }
  axios.defaults.headers['X-Api-Token'] = token
  axios.get(process.env.SERVER_API_URL + req.url, {
    headers:{
      'Content-Type': 'application/json'
    }
  })
    .then(data => returnResponse(res,200, data.data))
    .catch(err => returnResponse(res, err.response.status, err.response.data))
})


// RS Assistant

app.post('/v3/programs/rs-assistant/program-cases', async (req, res) => {
  let token
  try {
    token = await getToken(61)
  } catch (err){
    returnResponse(res, err.response.status)
    return
  }

  axios.defaults.headers['X-Api-Token'] = token

  axios.post(process.env.SERVER_API_URL + req.url,req.body,{
    headers: {
      'X-Api-Key': process.env.API_KEY_APP
    }
  })
    .then(data => returnResponse(res,200, data.data))
    .catch(err => {
      returnResponse(res, err.response.status)
    })
})
